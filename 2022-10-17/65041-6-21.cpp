#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <ctype.h>
#include <stdlib.h>
#include <bits/stdc++.h>
#include <string.h>
// #include <algorithm>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define mbr(n,m) get<m>(n)
void getString(char *question, char *answer) {
	char buffer[256];
	printf("%s ", question);
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%s", answer);
}
void getInt(char *question, int *answer) {
	printf("%s ", question);
	scanf("%d", answer);
}

int main() {
    int cid[16], z = 0, t;
	printf("Credit card number : "); scanf("%1d%1d%1d%1d-%1d%1d%1d%1d-%1d%1d%1d%1d-%1d%1d%1d%1d",&cid[0],&cid[1],&cid[2],&cid[3],&cid[4],&cid[5],&cid[6],&cid[7],&cid[8],&cid[9],&cid[10],&cid[11],&cid[12],&cid[13],&cid[14],&cid[15]);
    loop(16) {
        t = (i%2==0?2:1)*cid[i];
        z += (t>=10?t-9:t);
    } printf("Credit card number is %svalid.", (z%10==0?"":"in"));
    return 0;
}

/***
 * Test cases
 * 1) 4485-2757-4230-8327 | valid
 * 2) 6011-3299-3365-5299 | valid
 ***/
