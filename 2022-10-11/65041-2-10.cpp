#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <ctype.h>
#include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <algorithm>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int i = 0; i < n; i++)
#define mbr(n,m) get<m>(n)
void getString(char *question, char *answer) {
	char buffer[256];
	printf("%s ", question);
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%s", answer);
}
void getInt(char *question, int *answer) {
	printf("%s ", question);
	scanf("%d", answer);
}

int main() {
    int unit, price = 0;
    getInt("Total usage:", &unit);
    const pi cost[6] = {make_pair(10, 5), make_pair(40, 10), make_pair(50, 12), make_pair(400, 15), make_pair(500, 18), make_pair(4000, 20)};
    for (int pos = 0; pos < sizeof(cost); pos++) {
        price += (unit>cost[pos].st ? cost[pos].st : unit) * cost[pos].nd;
        unit = unit-cost[pos].st;
        if (unit <= 0) break;
    } if (unit > 0) price += unit*21;
    cout << "Total amount: " << price;
    return !0;
}
