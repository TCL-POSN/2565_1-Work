#include <stdio.h>
int main() {
    /*** Tryout 1
     * int X, Y, sum;
     * printf("Enter X: "); scanf("%d", &X);
     * printf("Enter Y: "); scanf("%d", &Y);
     * sum = X + Y;
     * printf("sum = %d\n", sum);
     * return 0;
     ***/

    /*** Tryout 2
     * int feet, inches;
     * printf("Enter number (in feet) ");
     * scanf("%d", &feet);
     * inches = feet*12;
     * printf("= %d inches\n", inches);
     * return 0;
     ***/

    /*** Tryout 3
     * int X, Y, sum;
     * printf("Enter X: "); scanf("%d", &X);
     * printf("Enter Y: "); scanf("%d", &Y);
     * sum = X + Y;
     * printf("sum = %d\n", sum);
     * return 0;
     ***/

    /*** Tryout 4
     * float r, C, PI = 3.1415926;
     * printf("Enter radius: ");
     * scanf("%f", &r);
     * C = 2*PI*r;
     * printf("Circumference = %f\n", C);
     ***/

    /*** Tryout 5
     * int X1, X2, X3;
     * float sum, mean;
     * printf(“Enter X1,X2,X3: ”);
     * scanf(“%d,%d,%d”, &X1,&X2,&X3);
     * sum = X1+X2+X3;
     * mean = sum/3;
     * printf(“Mean = %f\n”, mean);
     ***/

    /*** Tryout 6
     * int i, N, X;
     * float sum=0, mean;
     * printf(“Enter N: ”); scanf(“%d”, &N);
     * for (i=1;i<=N; i++) {
     *     printf(“Enter X%d= ”,i); scanf(“%d”, &X);
     *     sum = sum + X;
     * }
     * mean = sum/N;
     * printf(“Mean = %f\n”, mean);
     ***/

    /*** Tryout 7
     * int i;
     * for (i=1;i<=100; i++) {
     *     printf(“%d, ”, i);
     * }
     ***/

    /*** Tryout 8
     * int i, sum=0;
     * for (i=1;i<=100; i++) {
     *     printf(“%d+ ”, i);
     *     sum = sum + i;
     * }
     * printf(“ = %d\n”, sum);
     ***/

    /*** Tryout 9
     * int i;
     * float mul=1;
     * for (i=1;i<=10; i++) {
     *     printf(“%d x ”, i);
     *     mul = mul * i;
     * }
     * printf(“ = %f\n”, mul);
     * return 0;
     ***/

    /*** Tryout 10
     * int i, T=2, R;
     * for (i=1;i<=12; i++) {
     *     R = i * T;
     *     printf(“%d x %d = %d\n”, i,T,R);
     * }
     ***/

    return !0;
}