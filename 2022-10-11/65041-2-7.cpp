#include <stdio.h>
// #include <conio.h>
#include <math.h>
// #include <ctype.h>
#include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <algorithm>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int i = 0; i < n; i++)
#define mbr(n,m) get<m>(n)
void getString(char *question, char *answer) {
	char buffer[256];
	printf("%s ", question);
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%s", answer);
}
void getInt(char *question, int *answer) {
	printf("%s ", question);
	scanf("%d", answer);
}
#include <math.h>

int main() {
    int rad, menu;
    getInt("Please input radius:", &rad);
    cout << "Calculator menu:" << "\n\t1. Find area" << "\n\t2. Find circumference" << endl;
    getInt("Choose menu", &menu);
    if (menu-1) printf("Circumference = %.4f", 2*M_PI*rad);
    else printf("Area = %.4f", M_PI*pow(rad, 2));
    return !0;
}
