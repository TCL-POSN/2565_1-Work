#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <ctype.h>
#include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <algorithm>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int i = 0; i < n; i++)
#define mbr(n,m) get<m>(n)
void getString(char *question, char *answer) {
	char buffer[256];
	printf("%s ", question);
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%s", answer);
}
void getInt(char *question, int *answer) {
	printf("%s ", question);
	scanf("%d", answer);
}

int main() {
    int wage, ot;
    getInt("Employee ID:", &ot);
    if (ot < 10000 || ot > 99999) return !1;
    cout << "Wage & OT: "; cin >> wage >> ot;
    wage += ot;
    if (wage >= 1e5) wage *= 0.1;
    else if (wage >= 7e4) wage *= 0.07;
    else if (wage >= 5e4) wage *= 0.05;
    else if (wage >= 3e4) wage *= 0.03;
    else wage *= 0.01;
    cout << "Bonus " << wage << "!!";
    return !0;
}
