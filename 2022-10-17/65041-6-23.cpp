#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <ctype.h>
#include <stdlib.h>
#include <bits/stdc++.h>
#include <string.h>
// #include <algorithm>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define mbr(n,m) get<m>(n)
void getString(char *question, char *answer) {
	char buffer[256];
	printf("%s ", question);
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%s", answer);
}
void getInt(char *question, int *answer) {
	printf("%s ", question);
	scanf("%d", answer);
}

int main() {
    char s[2][20]; int ml;
	printf("Enter a word (1/2) : "); scanf("%s", s[0]);
	printf("Enter a word (2/2) : "); scanf("%s", s[1]);
	printf("8.1) "); ml = strlen(s[0])>strlen(s[1])?strlen(s[0]):strlen(s[1]);
    for (int i = 0; i<ml; i++) {
        if (i%2==1 && i<strlen(s[0])) printf("%c", s[0][i]);
        if (i%2==1 && i<strlen(s[1])) printf("%c", s[1][i]);
    } printf("\n8.2) ");
    for (int j = 0; j<ml; j++) {
        if (j%2==0 && j<strlen(s[0])) printf("%c", s[0][j]);
        if (j%2==0 && j<strlen(s[1])) printf("%c", s[1][j]);
    } printf("\n8.3) ");
    for (int k = 0; k<ml; k++) {
        if (k<strlen(s[0])) printf("%c", s[0][k]);
        if (k<strlen(s[1])) printf("%c", s[1][k]);
    } printf("\n8.4) CAP : ");
    for (int l = 0; l<strlen(s[0]); l++) printf("%c", s[0][l]-((s[0][l]>=97 && s[0][l]<=122)?32:0)); printf(" ");
    for (int m = 0; m<strlen(s[1]); m++) printf("%c", s[1][m]-((s[1][m]>=97 && s[1][m]<=122)?32:0));
    printf("\n     LOW : ");
    for (int n = 0; n<strlen(s[0]); n++) printf("%c", s[0][n]+((s[0][n]>=65 && s[0][n]<=90)?32:0)); printf(" ");
    for (int o = 0; o<strlen(s[1]); o++) printf("%c", s[1][o]+((s[1][o]>=65 && s[1][o]<=90)?32:0));
    printf("\n8.5) ");
    for (int p = 0; p<strlen(s[0]); p++) printf("%c", s[0][p]+((s[0][p]>=65 && s[0][p]<=90)?32:((s[0][p]>=97 && s[0][p]<=122)?-32:0))); printf(" ");
    for (int q = 0; q<strlen(s[1]); q++) printf("%c", s[1][q]+((s[1][q]>=65 && s[1][q]<=90)?32:((s[1][q]>=97 && s[1][q]<=122)?-32:0)));
    return 0;
}
