#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <ctype.h>
#include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <algorithm>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int i = 0; i < n; i++)
#define mbr(n,m) get<m>(n)
void getString(char *question, char *answer) {
	char buffer[256];
	printf("%s ", question);
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%s", answer);
}
void getInt(char *question, int *answer) {
	printf("%s ", question);
	scanf("%d", answer);
}

int fgcd(int a, int b) {
    while (b) {
        int t = b; b = a%b; a = t;
    } return a;
}
int flcm(int a, int b) {
    return !a || !b ? 0 : a*b/fgcd(a, b);
}
int main() {
    int div1, div2;
    getInt("Divider 1:", &div1);
    getInt("Divider 2:", &div2);
    div1 = flcm(div1, div2);
    for (int i = 1; i <= 1000; i++)
        if (i%div1==0) printf("%5d", i);
    return !0;
}
