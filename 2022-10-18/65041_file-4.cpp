#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <ctype.h>
#include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <algorithm>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define mbr(n,m) get<m>(n)
void getString(char *question, char *answer) {
	char buffer[256];
	printf("%s ", question);
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%s", answer);
}
void getInt(char *question, int *answer) {
	printf("%s ", question);
	scanf("%d", answer);
}

FILE* openFile(char pathname[], char mode[]) {
    return (fopen(pathname, "r")) ? fopen(pathname, mode) : fopen(pathname, "w");
}
char pathname[] = "tmp/campmates.tsv"; FILE *datafile;
#define COL 4
void tblHead() {
    printf("\n%-10s %c%-10s %c%-15s %c%s\n", "Telephone", 179, "Nickname", 179, "Firstname", 179, "Lastname");
    loop(11) printf("%c", 196); printf("%c", 197);
    loop(11) printf("%c", 196); printf("%c", 197);
    loop(16) printf("%c", 196); printf("%c", 197);
    loop(20) printf("%c", 196); cout << endl;
}
void tblRow(string info[]) {
    char buffer[32];
    strcpy(buffer, info[0].c_str()); printf("%-10s ", buffer);
    strcpy(buffer, info[1].c_str()); printf("%c%-10s ", 179, buffer);
    strcpy(buffer, info[2].c_str()); printf("%c%-15s ", 179, buffer);
    strcpy(buffer, info[3].c_str()); printf("%c%s\n", 179, buffer);
}
void addStudent() {
    char input[32]; datafile = openFile(pathname, "a");
    cout << endl;
    cout << "Enter phone number ";  cin >> input; fprintf(datafile, "%s\t", input);
    cout << "Enter nickname ";      cin >> input; fprintf(datafile, "%s\t", input);
    cout << "Enter firstname ";     cin >> input; fprintf(datafile, "%s\t", input);
    cout << "Enter lastname ";      cin >> input; fprintf(datafile, "%s\n", input);
    cout << endl; fclose(datafile);
}
void findStudent() {
    char buffer[256]; string query, info[COL]; datafile = openFile(pathname, "r");
    cout << "Enter term for search "; cin >> query;
    int result = 0, match;
    while (true) {
        match = 0;
        fgets(buffer, 256, datafile);
        if (feof(datafile)) break;
        stringstream ssin(buffer);
        for (int data = 0; ssin.good() && data < COL; data++) {
            ssin >> info[data];
            if (info[data].find(query) != string::npos) match = 1;
        } if (match == 1) {
            if (result == 0) tblHead();
            tblRow(info);
            result += 1;
        }
    } cout << ((result == 0) ? "No results" : "") << endl;
    fclose(datafile);
}
int main() {
    int option;
    do {
        cout << "Menu:\n\t1) New campmate (Add)\n\t2) Search campmate\n";
        do getInt("Choose...", &option); while (option < 0 || option > 2);
        switch (option) {
            case 1: addStudent(); break;
            case 2: findStudent(); break;
        }
    } while (option != 0);
    return 0;
}
