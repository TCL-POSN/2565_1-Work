#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <ctype.h>
#include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <algorithm>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int i = 0; i < n; i++)
#define mbr(n,m) get<m>(n)
void getString(char *question, char *answer) {
	char buffer[256];
	printf("%s ", question);
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%s", answer);
}
void getInt(char *question, int *answer) {
	printf("%s ", question);
	scanf("%d", answer);
}

int prime(int n) {
    int p = 1;
    if (n > 1) {
        for (int j = 2; j < n; j++) {
            if (n%j==0) { p = 0; break; }
        }
	} else p = 0;
	if (p) printf("%5d", n);
	return p;
}
int main() {
    int q, c = 0; getInt("Enter a number :", &q);
	if (q > 1) {
        for (int i = 2; i < q; i++) c += prime(i);
        cout << endl << "There are " << c << " prime numbers";
	} else cout << "No prime numbers";
    return !0;
}
