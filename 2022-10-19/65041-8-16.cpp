#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <ctype.h>
#include <time.h>
#include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <algorithm>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define mbr(n,m) get<m>(n)
void getString(char *question, char *answer) {
	char buffer[256];
	printf("%s ", question);
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%s", answer);
}
void getInt(char *question, int *answer) {
	printf("%s ", question);
	scanf("%d", answer);
}
int random(int start, int stop) {
    return rand()%stop+start;
}

#define ROUND 3
int main() {
    srand(time(NULL));
    int face, sum = 0; set<int> identical;
    loop(ROUND) {
        face = random(1, 6);
        printf("Dice %d: %d\n", i+1, face);
        sum += face;
        identical.insert(face);
    } printf("Three: %d\n", sum);
    if (identical.size() == 1) cout << "Three of a kind\n";
    cout << (sum > 10 ? "low" : "high");
    return 0;
}
