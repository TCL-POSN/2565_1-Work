#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <ctype.h>
#include <stdlib.h>
#include <bits/stdc++.h>
#include <string.h>
// #include <algorithm>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define mbr(n,m) get<m>(n)
void getString(char *question, char *answer) {
	char buffer[256];
	printf("%-15s ==> ", question);
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%s", answer);
}
void getInt(char *question, int *answer) {
	printf("%s ", question);
	scanf("%d", answer);
}

#define vowelAmt 5
#define borderSize 10
int main() {
    char input[256]; const char vowels[vowelAmt+1] = "AEIOU";
    printf("%-16s ==> ", "Input String"); gets(input);
    int length = strlen(input), vCounts[vowelAmt] = {};
    printf("%-16s ==> %d\n", "Length of String", length);
    printf("%-16s ==> ", "Reversed String");
    loop(length) cout << input[length-1-i]; cout << endl;
    printf("%-16s ==> ", "Upper String");
    loop(length) {
        if (input[i] >= 'a' && input[i] <= 'z') input[i] -= 32;
        cout << input[i];
        loop2(vowelAmt) if (input[i]==vowels[j]) { vCounts[j] += 1; break; }
    } cout << endl; loop(borderSize) cout << "=-"; cout << "=\nNumber of vowel\n"; loop(borderSize) cout << "=-"; cout << "=\n";
    loop(vowelAmt) printf("%c or %c = %d\n", vowels[i], vowels[i]+32, vCounts[i]);
    loop(borderSize) cout << "=-"; cout << "=";
    return 0;
}
