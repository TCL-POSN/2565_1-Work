#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <ctype.h>
#include <stdlib.h>
#include <bits/stdc++.h>
#include <string.h>
// #include <algorithm>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define mbr(n,m) get<m>(n)
void getString(char *question, char *answer) {
	char buffer[256];
	printf("%-16s ==> ", question);
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%s", answer);
}
void getInt(char *question, int *answer) {
	printf("%-16s ==> ", question);
	scanf("%d", answer);
}

int main() {
    char word[256], border; int height, width, length, thick;
    getString("Enter word", word);
    getInt("String height", &height);
    getInt("String width", &width);
    printf("%-16s ==> ", "Border character"); scanf("%s", &border);
    getInt("Border thickness", &thick);
    length = strlen(word);
    // Start drawing
    loop(thick) { loop2(2*thick + width*length) cout << border; cout << endl; }
    loop(height) {
        loop2(thick) cout << border;
        loop2(width) cout << word;
        loop2(thick) cout << border;
        cout << endl;
    } loop(thick) { loop2(2*thick + width*length) cout << border; cout << endl; }
    return 0;
}
