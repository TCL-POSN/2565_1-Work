#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <ctype.h>
#include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <algorithm>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define mbr(n,m) get<m>(n)
void getString(char *question, char *answer) {
	char buffer[256];
	printf("%s ", question);
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%s", answer);
}
void getInt(char *question, long unsigned *answer) {
	printf("%s ", question);
	scanf("%d", answer);
}

int main() {
    /* Bitwise straight way
    long unsigned input; bool start = false; string base2 = "", checker;
    getInt("Enter a number", &input);
    if (input <= 1) cout << input; else
    for (int digit = 29; digit >= 0; digit--) {
        checker = (input&pow(2, digit)>1 ? "1" : "0");
        if (!start && checker!="0") start = true;
        if (start) base2 = base2 + checker;
        if (checker!="0") {
            // input = input^pow(2, digit);
            input -= pow(2, digit);
            if (input == 0) break;
        }
    } cout << base2;
    */
    // Bitwise reverse way
    /* long unsigned input; string base2 = "", checker;
    getInt("Enter a number", &input);
    loop(30) {
        checker = (input&pow(2, digit)>1 ? "1" : "0");
        base2 = checker + base2;
        if (checker!="0") {
            input = input >> 1;
            if (input == 0) break;
        }
    } cout << stoi(base2);
    */
    // Dividing remainder
    long unsigned input; int checker; bool start = true; string base2 = "";
    getInt("Enter a number", &input);
    if (input <= 1) cout << input; else {
        while (input > 1) {
            checker = input%2;
            if (!start && checker>0) start = true;
            if (start) base2 = (checker==0?"0":"1") + base2;
            input = floor(input/2);
        } cout << "1" << base2;
    }
    return !0;
}
