#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <ctype.h>
#include <stdlib.h>
#include <bits/stdc++.h>
#include <string.h>
// #include <algorithm>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define mbr(n,m) get<m>(n)
void getString(char *question, char *answer) {
	char buffer[256];
	printf("%s ", question);
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%s", answer);
}
void getInt(char *question, int *answer) {
	printf("%s ", question);
	scanf("%d", answer);
}

int main() {
    char input[256]; const char vowels[] = "AEIOUaeiou"; getString("Enter text", input);
    int length = strlen(input), amtNumeric = 0, amtAlphabetic = 0, amtVowel = 0;
    printf("%9s %d\n", "Length:", length);
    loop(length) {
        if ((input[i] >= 'A' && input[i] <= 'Z') || (input[i] >= 'a' && input[i] <= 'z')) {
            amtAlphabetic += 1;
            loop2(10) if (input[i]==vowels[j]) { amtVowel += 1; break; }
        } else if (input[i] >= '0' && input[i] <= '9') amtNumeric += 1;
    } printf("%9s %d\n", "Number:", amtNumeric);
    printf("%9s %d\n", "Alphabet:", amtAlphabetic);
    printf("%9s %d\n", "Vowel:", amtVowel);
    return 0;
}
