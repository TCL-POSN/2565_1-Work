#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <ctype.h>
#include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <algorithm>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define mbr(n,m) get<m>(n)
void getString(char *question, char *answer) {
	char buffer[256];
	printf("%s ", question);
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%s", answer);
}
void getInt(char *question, int *answer) {
	printf("%s ", question);
	scanf("%d", answer);
}

ll convertToDec(char numBase[], int base){
    ll numDec = 0; int digit = strlen(numBase);
    loop(digit) {
        int val = (int)numBase[digit-1-i];
        if (val>=48 && val<=57) val -= 48; // 0 = 48
        else if(val>=65 && val<=90) val -= 55; // 10 = A
        else if(val>=97 && val<=122) val -= 61; // 36 = a
        else if(val==43 || val==44) val += 19; // 62 = +
        numDec += val*pow(base, i);
        // printf("$~ %d*%d^%d\n", val, base, i);
    } return numDec;
}
string convertToBase(ll number, int base){
    if (number > 0) {
        int val = number%base;
        if (val>=0 && val<=9) val += 48; // 0 = 48
        else if(val>=10 && val<=35) val += 55; // 10 = A
        else if(val>=36 && val<=61) val += 61; // 36 = a
        else if(val=='+' || val=='/') val -= 19; // 62 = +
        // stringstream mover; mover << val; string sender; mover >> sender;
        return convertToBase(number/base, base) + (char)val;
    } else return "";
}
#define MAXLENGTH 3
int main() {
    int base; ll numDec; char numBase[MAXLENGTH];
    getInt("Enter number base:", &base); int base2 = base;
    cout << "Enter number 1: "; cin >> numBase; numDec = convertToDec(numBase, base2);
    cout << "Enter number 2: "; cin >> numBase; numDec += convertToDec(numBase, base2);
    cout << convertToBase(numDec, base2);
    return !0;
}
