#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <ctype.h>
#include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <algorithm>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define mbr(n,m) get<m>(n)
void getString(char *question, char *answer) {
	char buffer[256];
	printf("%s ", question);
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%s", answer);
}
void getInt(char *question, int *answer) {
	printf("%s ", question);
	scanf("%d", answer);
}

#define LENGTH 32
bool isAnagram(char w1[], char w2[]) {
    int length = strlen(w1);
    if (strlen(w2) != length) return false;
    char word1[length+1], word2[length+1];
    strcpy(word1, w1); strcpy(word2, w2);
    sort(word1, word1+length); sort(word2, word2+length);
    char *pos1, *pos2; pos2 = word2;
    for (pos1 = word1; *pos1 != '\0'; pos1++) {
        if (*pos1 != *pos2++) return false;
    } return true;
}
int main() {
    char word1[LENGTH], word2[LENGTH];
    getString("Enter word 1:", word1);
    getString("Enter word 2:", word2);
    cout << (isAnagram(word1, word2) ? "yes" : "no");
    return 0;
}
