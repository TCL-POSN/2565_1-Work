#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <ctype.h>
#include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <algorithm>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int i = 0; i < n; i++)
#define mbr(n,m) get<m>(n)
void getString(char *question, char *answer) {
	char buffer[256];
	printf("%s ", question);
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%s", answer);
}
void getInt(char *question, int *answer) {
	printf("%s ", question);
	scanf("%d", answer);
}

#define AMOUNT 5
int main() {
    int store[AMOUNT], input, sum=0, val_max, val_min;
    loop(AMOUNT) {
        cout << "Enter a number [" << i+1 << "]: "; cin >> input;
        store[i] = input;
        if (i == 0) {
            val_max = input;
            val_min = input;
        } else {
            val_max = max(val_max, input);
            val_min = min(val_min, input);
        } sum += input;
    } cout << "Max: " << val_max << endl;
    cout << "Min: " << val_min << endl;
    cout << "Avg: " << (float)sum/AMOUNT;
    return !0;
}
