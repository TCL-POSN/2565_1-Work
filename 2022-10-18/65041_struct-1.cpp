#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <ctype.h>
#include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <algorithm>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define mbr(n,m) get<m>(n)
void getString(char *question, char *answer) {
	char buffer[256];
	printf("%s ", question);
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%s", answer);
}
void getInt(char *question, int *answer) {
	printf("%s ", question);
	scanf("%d", answer);
}

struct date {
    int d;
    int m;
    int y;
}; struct student {
    int ID;
    char fullname[40];
    struct date birth;
    struct date checkin;
    int room;
}; typedef struct student STD;
#define AMOUNT 5
STD stdrecord[AMOUNT] = {
    {10001, "Chris Hemsworth",  11, 12, 2552, 3, 5, 2560, 4},
    {10002, "Tom Cruise",       3,  4,  2552, 6, 5, 2555, 4},
    {10003, "Taylor Swift",     13, 12, 2552, 1, 5, 2560, 2},
    {10004, "Hugh Grant",       9,  9,  2551, 1, 5, 2560, 1},
    {10005, "Kristen Stewart",  9,  4,  2552, 7, 5, 2560, 1}
};

void tblHead() {
    printf("%-5s\t%c%-15s\t%c%-10s\t%c%-10s\t%c%s\n", "ID", 179, "Name", 179, "Birthdate", 179, "Checkin", 179, "Room");
    loop(8) printf("%c", 196); printf("%c", 197);
    loop(23) printf("%c", 196); printf("%c", 197);
    loop(15) printf("%c", 196); printf("%c", 197);
    loop(15) printf("%c", 196); printf("%c", 197);
    loop(4) printf("%c", 196); cout << endl;
}
void tblRow(STD *student, int record) {
    loop(record) student++;
    printf("%5d\t", student -> ID);
    printf("%c%-15s\t", 179, student -> fullname);
    printf("%c%2d %2d %4d\t", 179, (student -> birth).d, (student -> birth).m, (student -> birth).y);
    printf("%c%2d %2d %4d\t", 179, (student -> checkin).d, (student -> checkin).m, (student -> checkin).y);
    printf("%c%d\n", 179, student -> room);
}
int main() {
    int minimum = AMOUNT-1;
    loop(AMOUNT-1) {
        if (stdrecord[i].checkin.y > stdrecord[minimum].checkin.y) minimum = i;
        else if (stdrecord[i].checkin.y == stdrecord[minimum].checkin.y) {
            if (stdrecord[i].checkin.m > stdrecord[minimum].checkin.m) minimum = i;
            else if (stdrecord[i].checkin.m == stdrecord[minimum].checkin.m)
                if (stdrecord[i].checkin.d > stdrecord[minimum].checkin.d) minimum = i;
    } } tblHead();
    tblRow(stdrecord, minimum);
    return 0;
}
