#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <ctype.h>
#include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <algorithm>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int i = 0; i < n; i++)
#define mbr(n,m) get<m>(n)
void getString(char *question, char *answer) {
	char buffer[256];
	printf("%s ", question);
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%s", answer);
}
void getInt(char *question, int *answer) {
	printf("%s ", question);
	scanf("%d", answer);
}

int main() {
    int sc_mid, sc_fnl;
    getInt("Please enter midterm score :", &sc_mid);
    if (sc_mid < 0 || sc_mid > 100) return !1;
    getInt("Please enter final score :", &sc_fnl);
    if (sc_fnl < 0 || sc_fnl > 100) return !1;
    sc_fnl = (sc_mid+sc_fnl)*0.5;
    cout << "Your score = " << sc_fnl << "%" << endl;
    if (sc_fnl >= 80) cout << "Grade = P , Pass";
    else if (sc_fnl >= 50) cout << "Grade = G , Good";
    else cout << "Grade = F, Fail";
    return !0;
}
