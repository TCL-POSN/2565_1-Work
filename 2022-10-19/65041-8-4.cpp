#include <stdio.h>
// #include <conio.h>
#include <math.h>
// #include <ctype.h>
#include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <algorithm>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define mbr(n,m) get<m>(n)
void getString(char *question, char *answer) {
	char buffer[256];
	printf("%s ", question);
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%s", answer);
}
void getInt(char *question, int *answer) {
	printf("%s ", question);
	scanf("%d", answer);
}

void tRatio(double theta) {
    if ((int)theta%180==90) printf("%3.4f %c%3.4f %c%c\n", 1.0, 179, 0.0, 179, 236);
    else {
        theta *= M_PI / 180;
        printf("%3.4f %c%3.4f %c%3.4f\n", sin(theta), 179, cos(theta), 179, tan(theta));
    }
}
int main() {
    printf("  sin  %c  cos  %c  tan\n", 179, 179);
    loop(7) printf("%c", 196); printf("%c", 197);
    loop(7) printf("%c", 196); printf("%c", 197);
    loop(7) printf("%c", 196); cout << endl;
    for (int theta = 0; theta <= 90; theta += 5) tRatio((double)theta);
    return 0;
}
