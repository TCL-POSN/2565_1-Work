#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <ctype.h>
#include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <algorithm>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define mbr(n,m) get<m>(n)
void getString(char *question, char *answer) {
	char buffer[256];
	printf("%s ", question);
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%s", answer);
}
void getInt(char *question, int *answer) {
	printf("%s ", question);
	scanf("%d", answer);
}

int main() {
    int base; do {
        getInt("Size (1-99):", &base);
    } while (base < 1 || base > 99); int current = base-1;
    loop(base) {
        loop2(2*i) cout << " ";
        printf("%c ", 65+current%26);
        if (current>0) {
            loop2(4*(base-i)-6) cout << " ";
            printf("%c ", 65+current%26);
        } loop2(4*i) cout << " ";
        printf("%-2d", current+1);
        if (current>0) {
            loop2(4*(base-i)-6) cout << " ";
            printf("%-2d", (current--)+1);
        } cout << endl;
    } loop(base-1) {
        loop2(2*(base-2-i)) cout << " ";
        printf("%c ", 65+(++current)%26);
        loop2(4*i+2) cout << " ";
        printf("%c ", 65+current%26);
        loop2(4*(base-2-i)) cout << " ";
        printf("%-2d", current+1);
        loop2(4*i+2) cout << " ";
        printf("%-2d\n", current+1);
    }
    return !0;
}
