#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <ctype.h>
#include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <algorithm>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int i = 0; i < n; i++)
#define mbr(n,m) get<m>(n)
void getString(char *question, char *answer) {
	char buffer[256];
	printf("%s ", question);
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%s", answer);
}
void getInt(char *question, int *answer) {
	printf("%s ", question);
	scanf("%d", answer);
}

int main() {
    int amount, identical = 1;
    getInt("Enter amount", &amount);
    int store[amount];
    loop(amount) { cout << "Enter a number [" << i+1 << "]: "; cin >> store[i]; }
    sort(store, store+amount);
    int mode1 = store[0], occurence1 = 0, mode2 = store[0], occurence2 = 0; list<int> modes;
    loop(amount) {
        if (i>0 && store[i]!=mode1 && store[i]!=mode2) {
            if (occurence2>occurence1) {
                occurence1 = occurence2;
                mode1 = mode2;
                modes.clear();
            } else if (occurence1==occurence2 && occurence1>1) modes.push_back(mode2);
            mode2 = store[i];
            occurence2 = 0;
            identical++;
        } if (store[i]==mode1) occurence1++;
        else {
            occurence2++;
            if (i+1==amount && amount>1) {
                if (occurence2>occurence1) {
                    occurence1 = occurence2;
                    mode1 = mode2;
                    modes.clear();
                } else if (occurence1==occurence2 && occurence1>1) modes.push_back(mode2);
            }
        }
    } if (amount==1 || identical==1) cout << "Mode is " << store[0];
    else if (/*identical>1 &&*/ modes.size()+1<identical && occurence1>1) {
        if (modes.size()==1) {
            cout << "Mode are " << mode1;
            for (list<int>::iterator iter = modes.begin(); iter != modes.end(); iter++) cout << " and " << *iter;
        } else if (modes.size()>1) cout << "More than 2 eligible => no mode";
        else cout << "Mode is " << mode1;
    } else cout << "No mode";
    return !0;
}
