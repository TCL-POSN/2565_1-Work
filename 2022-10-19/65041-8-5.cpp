#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <ctype.h>
#include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <algorithm>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define mbr(n,m) get<m>(n)
#define len(a) (sizeof(a)/sizeof(*a))
void getString(char *question, char *answer) {
	char buffer[256];
	printf("%s ", question);
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%s", answer);
}
void getInt(char *question, int *answer) {
	printf("%s ", question);
	scanf("%d", answer);
}

int amin(int store[], int *amount) {
    int /*amount = len(store),*/ valMin = store[0];
    loop(*amount-1) valMin = min(valMin, store[i+1]);
    return valMin;
}
int amax(int store[], int *amount) {
    int /*amount = len(store),*/ valMax = store[0];
    loop(*amount-1) valMax = max(valMax, store[i+1]);
    return valMax;
}
int main() {
    int amount; getInt("Enter amount", &amount);
    int store[amount];
    loop(amount) { cout << "Enter number [" << i+1 << "]: "; cin >> store[i]; }
    printf("Min = %d\nMax = %d", amin(store, &amount), amax(store, &amount));
    return 0;
}
